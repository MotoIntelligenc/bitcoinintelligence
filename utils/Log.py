#!/usr/bin/env python
# Copyrite (c) 2017, Lei Wang
# All rights reserved.
# Author yiak.wy@gmail.com
import sys 
from os.path import join, abspath, dirname
root = abspath(join("..", dirname(__file__)))
sys.path.insert(0, root)

import logging 
from logging.config import dictConfig

import settings 

def InitLogFrmConfig(config=None):
    if config is None:
        config = segttings.LOGGING
    dictConfig(config)

class LogAdapter(logging.LoggerAdapter):

    def __init__(self, logger, prefix, **kwargs):
        logging.LoggerAdapter.__init__(self, logger, kwargs)
        self.prefix = prefix 

    def process(self, msg, kwargs):
        return "[%s] %s" % (self.prefix, msg), kwargs 


if __name__ == "__main__":
    from os.path import abspath, join, dirname
    root = abspath(join("..", dirname(__file__)))
    sys.path.insert(0, root)
    print(root)
