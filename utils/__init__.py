import os
from os.path import dirname, join, abspath 
import sys

root = abspath(join("..", dirname(__file__), "settings.py"))
sys.path.insert(0, root)

import settings
