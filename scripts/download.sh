#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${DIR}/start_py36ev.sh"
nohup python "${DIR}/spiders.py" > "${DIR}/nohup.out" &

