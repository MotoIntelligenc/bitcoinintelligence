# Copyrite (c) 2017, Lei Wang
# All rights reserved.
# Author yiak.wy@gmail.com

import sys 
from os.path import join, abspath, dirname
root = abspath(join("..", dirname(__file__)))
sys.path.insert(0, ".")
sys.path.insert(0, root)

from downloader import BaseSpider, Task, SimpleEventLoop

from lxml import html, etree 
from io import StringIO  

import logging
import  utils.Log as Log 
_logger = logging.getLogger("spiders")

import settings 

class MySpider(BaseSpider):
    
    logger = Log.LogAdapter(_logger, "MySpider")
    # sitemap relevant
    Rule1 = [
        "//div[@class='paginator']/a/@href",
        "//ul[@id='tag-sidebar']/li/a[2]/@href"
    ]

    Rule2 = [
        "//img[@class='preview']/@src",
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.target_number = 1000000000
        self.count = 0
        self.dirname = "../images"

    def parse_links(self, response):
        # import pdb; pdb.set_trace()
        parser = etree.HTMLParser()
        body = response.body.decode("utf-8")
        tree = etree.parse(StringIO(body), parser=parser)

        urls = []
        for rule in self.Rule1:
            for path in tree.xpath(rule):
                # the value will be sent to outer layer
                url = "http://" + "konachan.net" + path  
                yield url    
                urls.append(url)
    
        for rule in self.Rule2:
            for path in  tree.xpath(rule):
                url = "http:" + path
                yield url
                urls.append(url)

        return urls

def load_imgs():
    Log.InitLogFrmConfig(settings.LOGGING)
    root_url = "http://konachan.net/post?tags=aoi_tori"
    loop = SimpleEventLoop()
    loop.set_timeout(settings.TIME_OUT)
    spider = MySpider(root_url=root_url, max_redirect=3, max_depth=10, loop=loop)

    def routine(concurrency):
        tasks = [Task(spider._run()) for _ in range(concurrency)]

        yield from spider._q.join()
        for t in tasks:
            t.cancel()

    loop.run_until_complete(routine(1000))

if __name__ == "__main__":
    load_imgs()
