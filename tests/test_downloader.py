#!/usr/bin/env python
# Copyrite (c) 2017, Lei Wang
# All rights reserved.
# Author yiak.wy@gmail.com
import sys 
from os.path import join, abspath, dirname
root = abspath(join("..", dirname(__file__)))
sys.path.insert(0, root)

def test_download_with_asyn_urlopen():
    Log.InitLogFrmConfig(settings.LOGGING)
    loop = SimpleEventLoop()
    url = "http://konachan.net/image/4df6757f418f47dabe659ff043503d0c/Konachan.com%20-%20254793%20animal%20cat%20kazeno%20original.jpg"
    loop.run_until_complete(download(url, loop=loop))

if __name__ == "__main__":
    test_download_with_asyn_urlopen()
