# Copyrite (c) 2017, Lei Wang
# All rights reserved.
# Author yiak.wy@gmail.com

import sys
import os
import logging
_logger = logging.getLogger("downloader")
import utils.Log as Log 
import time

try:
    from urllib.request import urlopen
except:
    # py2
    from urllib2 import urlopen
# urlparse utility
try:
    from urllib.parse import urlparse, urlencode, quote_plus
except:
    # py2
    from urlparse import urlparse, urlencode, quote_plus

try:
    from asyncio import JoinableQueue as Queue
except ImportError:
    # py35+
    from asyncio import Queue 

# multiplexing through python interface
from asyncio import SelectorEventLoop, AbstractEventLoop
from selectors import DefaultSelector, EVENT_WRITE, EVENT_READ
import socket
import selectors

import settings 

class Handle:

    def __init__(self, cb, *args):
        self._cb = cb
        self._args = args
        self.logger = Log.LogAdapter(_logger, "Handle <%s>" % cb.__name__)

    def _run(self, fut):
        try:
            self._cb(fut, *self._args)
        except Exception as e:
            import traceback; traceback.print_exc()
            self.logger.info(e)
            raise(e)
# Simple Future class, reference https://github.com/python/cpython/blob/a6fba9b827e395fc9583c07bc2d15cd11f684439/Lib/asyncio/futures.py#L30:7
class Future:
    
    def __init__(self, *, loop=None):
        self._ret = None
        self._callbacks = []
        self._loop = loop
 
    def __iter__(self):
        yield self
        return self._ret

    def cancel(self):
        return True

    def add_done_callback(self, handle):
        self._callbacks.append(handle)

    def remove_done_callback(self, handle):
        if handle in self._callbacks:
            self._callbacks.remove(handle)

    def set_ret(self, ret):
        self._ret = ret
        self._execute_callbacks()

    def _execute_callbacks(self):
        callbacks = self._callbacks[:]
        self._callbacks[:] = []
        for h in callbacks:
            h._run(self)


# stop the task iteration 
class Cancel(Exception):pass 

class Task(Future):
    
    def __init__(self, coro, *, loop=None):
        super().__init__(loop=loop)
        self._coro = coro
        self._step(self)

    def cancel(self):
        self._coro.throw(Cancel())

    def _step(self, fut):
        try:
            ret = self._coro.send(fut._ret)
            if not isinstance(ret, Future) and not hasattr(ret, "add_done_callback"):
                ret0 = ret 
                while True:
                    ret1 = self._coro.send(ret0)
                    if isinstance(ret1, Future):
                        break
                    ret0 = ret1 
        except Cancel:
            # not useful for the moment
            # when user call Task.cancel this will return back to event loop
            self._cancelled = True
            return
        except StopIteration as e:
            self.set_ret(e.value)
            return
        except Exception as e:
            import traceback; traceback.print_exc()
            raise(e)
        h = Handle(self._step)
        if isinstance(ret, Future) or hasattr(ret, "add_done_callback"):
            next_fut = ret
        else:
            next_fut = ret1 
        next_fut.add_done_callback(h)
 
class StopEventLoop(Exception):pass
# https://github.com/python/cpython/blob/a6fba9b827e395fc9583c07bc2d15cd11f684439/Lib/asyncio/base_events.py#L232
class SimpleEventLoop(AbstractEventLoop):

    def __init__(self):
        self._stopped = False 
        self._stopping = False
        self._selector = DefaultSelector()
        self.logger = Log.LogAdapter(_logger, "SimpleEventLoop")

    def isStopping(self):
        return self._stopping

    def set_timeout(self, timeout):
        self.timeout = timeout

    def set_stopping(self, fut):
        self._stopping = False if self._stopping else True
        raise StopEventLoop 

    def run_until_complete(self, coro):
        task = Task(coro, loop=self)
        h = Handle(self.set_stopping)
        task.add_done_callback(h)
        try:
            self.run_forever()
        except StopImediately:
            pass 
        except StopEventLoop:
            pass 
        except Exception as e:
            import traceback;traceback.print_exc()
            rase(e)
        finally:
            self.logger.info("The event loop die.")
            task.remove_done_callback(h)

    def run_forever(self):
        while not self.isStopping():
            self._run_once()

    def _run_once(self):
        ready_evts = self._selector.select(self.timeout)
        self._process_evts(ready_evts)
    
    def _process_evts(self, ready_evts):
        for key, mask in ready_evts:
            (reader, writer) = key.data
            if mask & selectors.EVENT_READ and reader is not None:
                reader()
            if mask & selectors.EVENT_WRITE and writer is not None:
                writer()
    
    # used by standard asyncio.Queue
    def create_future(self):
        return Future(loop=self)

    def close(self):
        pass

    def is_closed(self):
        return self._stopped 

    def __del__(self):
        if not self.is_closed():
            self.close()

class Request:

    def __init__(self, method, url, 
            parsed_url=None, headers=None):
        self.url = url 
        self.parsed_url = parsed_url or urlparse(url)
        self.method = method

    def send(self, sock):
        _req_msg = '{} {} HTTP/1.0\r\nHost: {}\r\n\r\n'.format(self.method, self.url, self.parsed_url.hostname)
        sock.send(_req_msg.encode("utf-8"))
        return Response(self.method, self.url, sock)

class Response:

    def __init__(self, method, url, sock, loop=None):
        self.method = method
        self.url = url
        self.sock = sock
        self._loop = loop 
        # traditional response info, reference HTTPResponse 
        self._response = None
        self._chunked = None

    def __iter__(self):
        yield self

    def set_loop(self, loop):
        self._loop = loop 

    def _read(self, CHUNK):
        fut = Future()
        fd = self.sock.fileno()

        def onReadable():
            fut.set_ret(self.sock.recv(CHUNK))

        self._loop._selector.register(fd, EVENT_READ, (onReadable, None))
        chunk = yield from fut 
        self._loop._selector.unregister(fd)
        return chunk 

    def read(self, CHUNK=None):
        if self._chunked is not None and CHUNK is not None:
            return self._chunked   
        CHUNK = CHUNK or 8192
        ret = b""
        while True:
            chunk = yield from self._read(CHUNK)
            if not chunk:
                break
            ret += chunk
        self._chunked = ret
        self._parse(ret)
        return ret 

    def get_header(self, header):
        return self._response.getheader(header)

    @property 
    def status(self):
        return self._response.status 
    
    @property 
    def body(self):
        return self._response.read()     

    def _parse(self, req_txt):
        try:
            from httplib import HTTPResponse
        except:
            # py3
            from http.client import HTTPResponse
        try:
            from BytesIO import BytesIO
        except:
            # py3
            from io import BytesIO

        class _FakeSocket():

            def __init__(self, req_txt):
                self._fp = BytesIO(req_txt)
            # https://github.com/python/cpython/blob/c9758784eb321fb9771e0bc7205b296e4d658045/Lib/http/client.py#L217
            # https://stackoverflow.com/questions/24728088/python-parse-http-response-string 
            def makefile(self, *args, **kwargs):
                return self._fp
        
        fsock = _FakeSocket(req_txt)
        self._response = HTTPResponse(fsock)
        self._response.begin()

    def close(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

def asyn_urlopen(url, parsed_url=None, timeout=None, loop=None):
    logger = Log.LogAdapter(_logger, "asyn_urlopen")
    sock = socket.socket()
    sock.setblocking(False)
    loop.set_timeout(timeout)
    parsed = parsed_url or urlparse(url)
    addr = (parsed.hostname, parsed.port or 80)
    logger.info("connectting to addr (%s,%d)" % addr)
    try:
        sock.connect(addr)
    except BlockingIOError:
        pass

    fut = Future()

    def onConnected():
        fut.set_ret(None)
    
    fd = sock.fileno()
    loop._selector.register(fd, EVENT_WRITE, (None, onConnected))
    yield from fut
    logger.info("TCP connection built.")
    loop._selector.unregister(fd)
    # initiate http reuqest
    req = Request('GET', url, parsed_url=parsed)
    response = req.send(sock)
    response.set_loop(loop)
    return response

# dataset downloader
def download(url, timeout=settings.TIME_OUT, loop=None, 
            success_handler=None, err_handler=None, dirname=None):
    # TO DO: logger adpator
    logger = Log.LogAdapter(_logger, "download")
    logger.info("parsing url ...")
    parsed = urlparse(url)
    logger.info("url parsed.")
    filename = os.path.basename(parsed.path)
    if os.path.isfile(filename):
        logger.warn("The file has already existed")
    # connect I/O
    # write I/O, socket write http request to remote
    # response = urlopen(url, timeout=timeout)
    logger.info("downloading ...")
    start = time.time()
    response = yield from asyn_urlopen(url, timeout=timeout, loop=loop)
    elapsed = time.time() - start
    logger.info("connection built, eslapsed :%s sec" % elapsed)
    CHUNK = 8192
    
    dirname = dirname or "downloaded_data"
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    
    logger.info("writing data to local file ...")
    # import pdb; pdb.set_trace()
    with open(os.path.join(dirname, filename), "wb") as f:
        while True:
            # read I/O
            # import pdb; pdb.set_trace()
            chunk = yield from response._read(CHUNK)
            if not chunk:
                break
            f.write(chunk)
    logger.info("done.")

    response.close()
    return filename

def is_redirect(code):
    return code >= 300 and code <= 399

class StopCrawling(Exception):pass 
class StopImediately(Exception):pass  
# Created on 28th Nov 2017 by Lei
# Please refer to spcrapy for more info. If you want to konw more about Dynamic Rich Protected Content
# scraping, please keep an eye on my github page https://github.com/yiak.wy
class BaseSpider:

    def __init__(self, root_url, max_redirect, max_depth, loop=None):
        self._q  = Queue(maxsize=10000)
        self.root_url = root_url
        self.max_redirect = max_redirect
        self.max_depth = max_depth
        self._loop = loop 
        self.seen_urls = set()
        data = (root_url, max_redirect, 0)
        self._q.put_nowait(data)

    def _run(self):
        while True:
            # import pdb; pdb.set_trace()
            url, max_redirect, depth = yield from self._q.get()
            yield from self.crawl(url, max_redirect, 0)
            self._q.task_done()

    def parse_links(self, response):
        raise Exception("Not Implemented!")

    def crawl(self, url, max_redirect, depth):
        self.logger.info("crawling %s at depth %da, seen urls %d" % (url, depth, len(self.seen_urls)))
        parsed = urlparse(url)
        filename = os.path.basename(parsed.path)
        
        if filename is not None and filename.endswith(".jpg"):
            if self.count >= self.target_number:
                raise StopImediately()
            yield from download(url, dirname=self.dirname, loop=self._loop)
            self.count += 1
            return 
        
        try:
            response = yield from asyn_urlopen(url, parsed_url=parsed, timeout=settings.TIME_OUT, loop=self._loop)
            yield from response.read()
        # import pdb; pdb.set_trace()
            if depth >= self.max_depth:
                raise StopCrawling()
            if is_redirect(response.status):
                if max_redirect > 0: next_url = response.get_header('location')
                if next_url in self.seen_urls: return 

                self.seen_urls.add(next_url)
                data = (next_url, max_redirect-1, depth)
                self._q.put_nowait(data)
            else:
                links = yield from self.parse_links(response)
                for link in set(links).difference(self.seen_urls):
                    data = (link, self.max_redirect, depth+1)
                    self._q.put_nowait(data)
                self.seen_urls.update(links)
        except StopCrawling:
            self.logger.info("StopCrawing ...")
            pass 
        finally:
            response.close()
