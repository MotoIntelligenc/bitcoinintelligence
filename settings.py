"""
global settings for GAN project

Author: Lei Wang (yiak.wy@gmail.com)
Date: 2017/11/22
"""

import sys
import os

# downlaoder
TIME_OUT=15

# mail 
MAIL_ADDR=["mail.x.com", "devops@x.com", "notify@x.com"]
MAIL_CREDENTIALS=["devops@x.com", "password"]
MAIL_HOSTS="localhost"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "%(asctime)s [%(levelname)s]:%(filename)s, %(name)s, in line %(lineno)s >> %(message)s",
            'datefmt': "%a, %d, %b, %Y %H:%M:%S", #"%d/%b/%Y %H:%M:%S"
            }
    # add different formattrs here
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'verbose'
            },
        'email_remote': {
            'level': 'ERROR',
            'class': 'logging.handlers.SMTPHandler',
            'formatter': 'verbose',
            'mailhost': MAIL_HOSTS,
            'fromaddr': 'gan_notify@yiak.co',
            'toaddrs': MAIL_ADDR,
            'subject': 'GAN project ERROR!',
            'credentials': MAIL_CREDENTIALS

        },
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter':'verbose',
            'filename': './data/logs/GAN.log'
         }

    },
    'loggers': {
        'downloader': {
            'handlers': ['console', 'file'],
            'propagate': True,
            'level': 'INFO'
            },
        'GAN': {
            'handlers': ['console', 'file'],
            'propagate': True,
            'level': 'INFO'
            },
        'spiders': {
            'handlers': ['console', 'file'],
            'propagate': True,
            'level': 'INFO'
            }
    }

}
